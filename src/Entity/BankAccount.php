<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BankAccountRepository")
 */
class BankAccount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $account_number;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AccountType", inversedBy="bankAccounts", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $account_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bankAccounts", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="transmitter")
     */
    private $transactions;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    public function getId():  ? int
    {
        return $this->id;
    }

    public function getAccountNumber() :  ? string
    {
        return $this->account_number;
    }

    public function setAccountNumber(string $account_number) : self
    {
        $this->account_number = $account_number;

        return $this;
    }

    public function getBalance():  ? float
    {
        return $this->balance;
    }

    public function setBalance(float $balance) : self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getStatus():  ? bool
    {
        return $this->status;
    }

    public function setStatus(bool $status) : self
    {
        $this->status = $status;

        return $this;
    }

    public function getAccountType():  ? AccountType
    {
        return $this->account_type;
    }

    public function setAccountType( ? AccountType $account_type) : self
    {
        $this->account_type = $account_type;

        return $this;
    }

    public function getUser() :  ? User
    {
        return $this->user;
    }

    public function setUser( ? User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions() : Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setTransmitter($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getTransmitter() === $this) {
                $transaction->setTransmitter(null);
            }
        }

        return $this;
    }
}
