<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransactionType", inversedBy="transactions")
     */
    private $operation_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount", inversedBy="transactions")
     */
    private $transmitter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount", inversedBy="transactions")
     */
    private $receiver;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getOperationType(): ?TransactionType
    {
        return $this->operation_type;
    }

    public function setOperationType(?TransactionType $operation_type): self
    {
        $this->operation_type = $operation_type;

        return $this;
    }

    public function getTransmitter(): ?BankAccount
    {
        return $this->transmitter;
    }

    public function setTransmitter(?BankAccount $transmitter): self
    {
        $this->transmitter = $transmitter;

        return $this;
    }

    public function getReceiver(): ?BankAccount
    {
        return $this->receiver;
    }

    public function setReceiver(?BankAccount $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
