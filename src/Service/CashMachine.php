<?php

namespace App\Service;

use App\Entity\Transaction;
use App\Model\Bank\Account\Operation\Depositable;
use App\Model\Bank\Account\Operation\Payable;
use App\Model\Bank\Account\Operation\Transactionable;

class CashMachine
{
    /**
     * Debit
     */
    private $debit;

    /**
     * Credit
     */
    private $credit;

    /**
     * Transaction
     */
    private $transaction;

    public function __construct(
        Payable $credit,
        Depositable $debit,
        Transactionable $transaction
    ) {
        $this->debit = $debit;
        $this->credit = $credit;
        $this->transaction = $transaction;
    }

    /**
     * Call Operation
     *
     * @param  TransactionType $type
     * @param  float $amount
     * @param  BankAccount $transmitter
     * @param  BankAccount $receiver
     * @return mixed
     */
    public function doOperation($type, $amount, $transmitter, $receiver = null)
    {
        if (is_null($transmitter)) {
            throw new \Exception("The bank account does not exist.", 4300);
        }

        $accountType = strtolower($transmitter->getAccountType()->getName());
        $operation = strtolower($type->getName());

        if (in_array($operation, get_class_methods($this->{$accountType}))) {

            try {

                $transaction = $this->{$accountType}->{$operation}($amount, $transmitter);
                $this->transaction->persist($type, $amount, $transmitter, $receiver);

            } catch (\Exception $e) {

                throw new \Exception($e->getMessage());
            }

        } else {

            throw new \Exception("Transaction not allowed for account type.", 4500);
        }
    }
}
