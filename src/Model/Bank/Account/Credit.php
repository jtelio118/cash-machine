<?php

namespace App\Model\Bank\Account;

use App\Entity\BankAccount;
use App\Model\Bank\AccountType;
use App\Model\Bank\Account\Operation\Payable;
use App\Model\Bank\Account\Operation\Withdrawable;
use Doctrine\ORM\EntityManagerInterface;

class Credit implements Payable, Withdrawable
{
    /**
     * Entity Manager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Pay Operation
     *
     * @param  float       $amount
     * @param  BankAccount $transmitter
     * @param  BankAccount $receiver
     * @return mixed
     */
    public function pay(float $amount, BankAccount $transmitter, BankAccount $receiver)
    {
        if ($amount <= 0) {
            throw new \Exception("The quantity can not be less than 0.", 4400);
        }

        if ($amount > $transmitter->getBalance()) {
            throw new \Exception("Insufficient balance.", 4401);
        }

        if (AccountType::CREDIT_ACCOUNT_ID == $transmitter->getAccountType()) {
            throw new \Exception("You can only pay with a credit account.", 4402);
        }

        try {

            $transmitter->setBalance($transmitter->getBalance() - $amount);
            $receiver->setBalance($receiver->getBalance() + $amount);

            $this->em->persist($transmitter);
            $this->em->persist($receiver);

            $this->em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return true;
    }

    /**
     * Withdraw Operation
     *
     * @param  float       $amount
     * @param  BankAccount $account
     * @return void
     */
    public function withdraw(float $amount, BankAccount $account)
    {
        if ($amount <= 0) {
            throw new \Exception("The quantity can not be less than 0.", 4400);
        }

        if ($amount > $account->getBalance()) {
            throw new \Exception("Insufficient balance.", 4401);
        }

        $balance = $account->getBalance() - $this->getRate($amount);

        try {

            $account->setBalance($balance);

            $this->em->persist($account);
            $this->em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return true;
    }

    /**
     * Get the rate over the amount
     *
     * @param  float $amount
     * @return float
     */
    protected function getRate($amount): float
    {
        $rate = ($amount * AccountType::CREDIT_DEFAULT_RATE) / 100;

        return $amount + $rate;
    }
}
