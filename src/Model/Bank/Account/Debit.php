<?php

namespace App\Model\Bank\Account;

use App\Entity\BankAccount;
use App\Model\Bank\Account\Operation\Depositable;
use App\Model\Bank\Account\Operation\Withdrawable;
use Doctrine\ORM\EntityManagerInterface;

class Debit implements Depositable, Withdrawable
{
    /**
     * Entity Manager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Deposit Operation
     * @param  float       $amount
     * @param  BankAccount $receiver
     * @return mixed
     */
    public function deposit(float $amount, BankAccount $receiver)
    {
        if ($amount <= 0) {
            throw new \Exception("The quantity can not be less than 0.", 4400);
        }

        try {

            $receiver->setBalance($receiver->getBalance() + $amount);
            $this->em->persist($receiver);
            $this->em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return true;
    }

    /**
     * Withdraw Operation
     * @param  float       $amount
     * @param  BankAccount $account
     * @return mixed
     */
    public function withdraw(float $amount, BankAccount $account)
    {
        if ($amount <= 0) {
            throw new \Exception("The quantity can not be less than 0.", 4400);
        }

        if ($amount > $account->getBalance()) {
            throw new \Exception("Insufficient balance.", 4401);
        }

        $balance = $account->getBalance() - $amount;

        try {

            $account->setBalance($balance);

            $this->em->persist($account);
            $this->em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return true;
    }
}
