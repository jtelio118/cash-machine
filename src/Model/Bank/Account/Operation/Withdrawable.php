<?php

namespace App\Model\Bank\Account\Operation;

use App\Entity\BankAccount;

interface Withdrawable
{
    /**
     * Withdraw Action
     *
     * @param  float       $amount
     * @param  BankAccount $account
     * @return void
     */
    public function withdraw(float $amount, BankAccount $account);
}
