<?php

namespace App\Model\Bank\Account\Operation;

use App\Entity\BankAccount;

interface Payable
{
    /**
     * Pay Action
     *
     * @param  float       $amount
     * @param  BankAccount $transmitter
     * @param  BankAccount $receiver
     * @return void
     */
    public function pay(float $amount, BankAccount $transmitter, BankAccount $receiver);
}
