<?php

namespace App\Model\Bank\Account\Operation;

use App\Entity\BankAccount;

interface Depositable
{
    /**
     * Deposit Action
     *
     * @param  float       $amount
     * @param  BankAccount $receiver
     * @return void
     */
    public function deposit(float $amount, BankAccount $receiver);
}
