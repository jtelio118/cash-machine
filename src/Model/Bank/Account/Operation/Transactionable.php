<?php

namespace App\Model\Bank\Account\Operation;

use App\Entity\BankAccount;
use App\Entity\TransactionType;

interface Transactionable
{
    /**
     * Transaction Save
     *
     * @param  TransactionType $operationType
     * @param  float           $amount
     * @param  BankAccount     $transmitter
     * @param  BankAccount     $receiver
     * @return void
     */
    public function persist(TransactionType $operationType, float $amount, BankAccount $transmitter, BankAccount $receiver);
}
