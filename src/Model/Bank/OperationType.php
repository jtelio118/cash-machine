<?php

namespace App\Model\Bank;

class OperationType
{
    /**
     * Charge Operation
     */
    const CHARGE_OPERATION = 'charge';

    /**
     * Credit Account
     */
    const PAY_OPERATION = 'pay';

    /**
     * Type of account
     */
    const DEPOSIT_OPERATION = 'deposit';

    /**
     * Type of account
     */
    const WITHDRAW_OPERATION = 'withdraw';

    /**
     * List of operation types (Defalt)
     */
    const OPERATION_TYPE_LIST = [
        self::CHARGE_OPERATION,
        self::PAY_OPERATION,
        self::DEPOSIT_OPERATION,
        self::WITHDRAW_OPERATION
    ];
}
