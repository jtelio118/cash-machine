<?php

namespace App\Model\Bank;

class AccountType
{
    /**
     * Debit Account
     */
    const DEBIT_ACCOUNT = 'debit';

    /**
     * Credit Account
     */
    const CREDIT_ACCOUNT = 'credit';

    /**
     * Type of account
     */
    const DEBIT_ACCOUNT_ID = 1;

    /**
     * Type of account
     */
    const CREDIT_ACCOUNT_ID = 2;


    /**
     * Interest rate by operation
     */
    const CREDIT_DEFAULT_RATE = 10;

    /**
     * List of account types (Defalt)
     */
    const ACCOUNT_TYPE_LIST = [
        self::DEBIT_ACCOUNT,
        self::CREDIT_ACCOUNT
    ];
}
