<?php

namespace App\Model\Bank;


use App\Entity\BankAccount;
use App\Entity\Transaction as TransactionEntity;

use App\Model\Bank\Account\Operation\Transactionable;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;

class Transaction implements Transactionable
{
    /**
     * Entity Manager
     */
    private $em;

    /**
     * Logger Manager
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Save Transaction
     * @param  TransactionType $operation
     * @param  float $amount
     * @param  BankAccount $transmitter
     * @param  BankAccount $receiver
     * @return void
     */
    public function persist($operation, $amount, $transmitter, $receiver = null)
    {
        $transaction = new TransactionEntity();
        $transaction
            ->setOperationType($operation)
            ->setAmount($amount)
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setDate(new \DateTime('now'));

        try {

            $this->em->persist($transaction);
            $this->em->flush();

        } catch (\Exception $e) {

            $this->logger->error($e->getMessage());
            return $e->getMessage();

        }
    }
}
