<?php

namespace App\DataFixtures;

use App\Entity\BankAccount;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class BankAccountFixtures extends BaseFixture implements DependentFixtureInterface
{

    const ACCOUNT_REFERENCE = 'bank_account_';

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(BankAccount::class, 30, function (BankAccount $bankAccount, $count) {

            $bankAccount->setAccountNumber($this->faker->numberBetween(100000, 999999))
                ->setBalance((float) $this->faker->numberBetween(100, 99999))
                ->setAccountType(
                    $this->getReference(AccountTypeFixtures::ACCOUNT_TYPE_REFERENCE . $this->faker->numberBetween(0, 1))
                )
                ->setUser(
                    $this->getReference(UserFixtures::USER_REFERENCE . $this->faker->numberBetween(1, 15))
                )
                ->setStatus($this->faker->boolean);

            $this->setReference(self::ACCOUNT_REFERENCE . $count, $bankAccount);
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AccountTypeFixtures::class,
            UserFixtures::class,
        ];
    }
}
