<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends BaseFixture
{
    const USER_REFERENCE = 'user_';

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, 80, function (User $user, $count) {

            $name = sprintf('%s %s', $this->faker->firstName, $this->faker->lastName);
            $birthdate = new \DateTime('now');
            $year = $this->faker->numberBetween(17, 60);

            $user->setUsername($this->faker->userName)
                ->setBirthdate($birthdate->modify(sprintf('-%d year', $year)))
                ->setName($name)
                ->setPlainPassword('123456')
                ->setEmail($this->faker->email)
                ->setEnabled($this->faker->boolean)
                ->setRoles(['ROLE_USER'])
                ->setSuperAdmin(false);

            $this->setReference(self::USER_REFERENCE . $count, $user);
        });

        $manager->flush();
    }
}
