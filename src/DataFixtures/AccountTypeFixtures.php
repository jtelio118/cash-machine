<?php

namespace App\DataFixtures;

use App\Entity\AccountType;
use App\Model\Bank\AccountType as AccountTypeList;

use Doctrine\Common\Persistence\ObjectManager;

class AccountTypeFixtures extends BaseFixture
{
    const ACCOUNT_TYPE_REFERENCE = 'account_type_';

    protected function loadData(ObjectManager $manager)
    {
        foreach (AccountTypeList::ACCOUNT_TYPE_LIST as $key => $type) {

            $accountType = new AccountType();
            $accountType
                ->setName(ucfirst($type))
                ->setDescription(sprintf('%s Account', ucfirst($type)));

            $manager->persist($accountType);

            $this->setReference(self::ACCOUNT_TYPE_REFERENCE . $key, $accountType);
        }

        $manager->flush();
    }
}
