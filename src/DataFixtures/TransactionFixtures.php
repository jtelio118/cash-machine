<?php

namespace App\DataFixtures;

use App\Entity\Transaction;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TransactionFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Transaction::class, 30, function (Transaction $transaction, $count) {

            $transaction->setAmount((float) $this->faker->numberBetween(1, 1000))
                ->setOperationType(
                    $this->getReference(TransactionTypeFixtures::TRANSACTION_TYPE_REFERENCE . $this->faker->numberBetween(0, 3))
                )
                ->setTransmitter(
                    $this->getReference(BankAccountFixtures::ACCOUNT_REFERENCE . $this->faker->numberBetween(1, 15))
                )
                ->setReceiver(
                    $this->getReference(BankAccountFixtures::ACCOUNT_REFERENCE . $this->faker->numberBetween(1, 15))
                )
                ->setDate(new \DateTime('now'));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AccountTypeFixtures::class,
            BankAccountFixtures::class,
            TransactionTypeFixtures::class,
        ];
    }
}
