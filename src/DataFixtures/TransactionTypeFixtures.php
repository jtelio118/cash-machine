<?php

namespace App\DataFixtures;

use App\Model\Bank\OperationType;
use App\Entity\TransactionType;
use Doctrine\Common\Persistence\ObjectManager;

class TransactionTypeFixtures extends BaseFixture
{
    const TRANSACTION_TYPE_REFERENCE = 'transaction_type_';

    protected function loadData(ObjectManager $manager)
    {
        foreach (OperationType::OPERATION_TYPE_LIST as $key => $type) {

            $accountType = new TransactionType();
            $accountType
                ->setName(ucfirst($type))
                ->setDescription(sprintf('%s Transaction', ucfirst($type)));

            $manager->persist($accountType);

            $this->setReference(self::TRANSACTION_TYPE_REFERENCE . $key, $accountType);
        }

        $manager->flush();
    }
}
