<?php

namespace App\Controller\Api\TransactionType;

use App\Entity\TransactionType;
use App\Service\SerializerHelper;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/operation")
 */
class ListController extends AbstractController
{
    /**
     * List Transaction Types
     *
     * @Route("/list", name="api_transaction_type_list", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return transaction types",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TransactionType::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="Transactions")
     *
     * @param SerializerHelper $serializerHelper
     * @return JsonResponse
     */
    function list(SerializerHelper $serializerHelper): JsonResponse{
        $list = [];

        foreach ($this->_getTransactionTypes() as $operation) {
            $list[] = [
                'id' => $operation->getId(),
                'name' => $operation->getName(),
                'description' => $operation->getDescription(),
            ];
        }

        return new JsonResponse($serializerHelper->serialize($list), 200);
    }

    /**
     * Get All Transactions
     * @return ManagerRegistry
     */
    protected function _getTransactionTypes()
    {
        $repository = $this->getDoctrine()->getRepository(TransactionType::class);
        return $repository->findAll();
    }
}
