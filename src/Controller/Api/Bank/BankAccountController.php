<?php

namespace App\Controller\Api\Bank;

use App\Entity\BankAccount;
use App\Service\SerializerHelper;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/account")
 */
class BankAccountController extends AbstractController
{
    /**
     * Account Detail
     *
     * @Route("/{id}", name="api_bankaccount_detail", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return Bank Account",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=BankAccount::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="Bank Account")
     * @Security(name="Bearer")
     *
     * @param BankAccount $account
     * @param SerializerHelper $serializerHelper
     * @return JsonResponse
     */
    public function detail(BankAccount $account, SerializerHelper $serializerHelper): ?JsonResponse
    {
        $accountInfo = [
            'id' => $account->getId(),
            'account_number' => $account->getAccountNumber(),
            'balance' => $account->getBalance(),
            'account_type' => $account->getAccountType()->getName(),
            'transactions' => $account->getTransactions()->map(
                function($transaction) {
                    return [
                        'id' => $transaction->getId(),
                        'operation_type' => $transaction->getOperationType()->getName(),
                        'amount' => $transaction->getAmount(),
                    ];
                }
            )
        ];

        return new JsonResponse($serializerHelper->serialize($accountInfo), 200);
    }
}
