<?php

namespace App\Controller\Api\User;

use App\Entity\User;
use App\Service\SerializerHelper;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class DetailController extends AbstractController
{
    /**
     * User Detail
     *
     * @Route("/{id}", name="api_user_detail", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns user",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     *
     * @param User $user
     * @param SerializerHelper $serializerHelper
     * @return JsonResponse
     */
    public function detail(User $user, SerializerHelper $serializerHelper):  ? JsonResponse
    {
        try {
            $this->denyAccessUnlessGranted('view', $user);

        } catch (\Exception $e) {

            return new JsonResponse($serializerHelper->serialize([
                'error' => $e->getMessage(),
            ]), 500);
        }

        $userInfo = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'username' => $user->getUsername(),
            'birthdate' => (string) $user->getBirthdate()->format('Y-m-d H:i:s'),
            'email' => $user->getEmail(),
            'accounts' => $user->getBankAccounts()->map(
                function ($account) {
                    return [
                        'id' => $account->getId(),
                        'balance' => $account->getBalance(),
                        'account_type' => $account->getAccountType()->getName(),
                    ];
                }
            ),
        ];

        return new JsonResponse($serializerHelper->serialize($userInfo), 200, []);
    }
}
