<?php

namespace App\Controller\Api\User;

use App\Entity\User;
use App\Service\SerializerHelper;
use FOS\UserBundle\Model\UserManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * @Route("/auth")
 */
class RegisterController extends AbstractController
{
    /**
     * User Register
     *
     * @Route("/register", name="api_auth_register", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     @SWG\Property(property="operation", type="object",
     *         @SWG\Property(
     *             property="username",
     *             type="string",
     *             description="username"
     *         ),
     *         @SWG\Property(
     *             property="name",
     *             description="customer name",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="email",
     *             description="customer email",
     *             type="string"
     *         ),
     *         @SWG\Property(
     *             property="password",
     *             description="Password",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Register user",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @param UserManagerInterface $userManager
     * @param SerializerHelper $serializerHelper
     * @return JsonResponse
     */
    public function register(
        Request $request,
        UserManagerInterface $userManager,
        SerializerHelper $serializerHelper ): ?JsonResponse
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection([
            'username' => new Assert\Length(['min' => 1]),
            'password' => new Assert\Length(['min' => 1, 'max' => 15]),
            'email' => new Assert\Email(),
            'name' => new Assert\Length(['min' => 1]),
        ]);

        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse($serializerHelper->serialize([
                'error' => (string) $violations,
            ]), 422);
        }

        $user = new User();
        $user
            ->setUsername($data['username'])
            ->setPlainPassword($data['password'])
            ->setEmail($data['email'])
            ->setName($data['name'])
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setSuperAdmin(false);

        try {

            $userManager->updateUser($user, true);

        } catch (\Exception $e) {

            return new JsonResponse($serializerHelper->serialize([
                'error' => $e->getMessage(),
            ]), 500);
        }

        return new JsonResponse($serializerHelper->serialize([
            'success' => 'The user was created successfully.',
        ]), 201);
    }
}
