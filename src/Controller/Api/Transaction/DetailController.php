<?php

namespace App\Controller\Api\Transaction;

use App\Entity\Transaction;
use App\Service\SerializerHelper;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/transaction")
 */
class DetailController extends AbstractController
{
    /**
     * Transaction Detail
     *
     * @Route("/{id}", name="api_transaction_detail", methods={"GET"})
     *  @SWG\Response(
     *     response=200,
     *     description="Return transaction",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="Transactions")
     * @Security(name="Bearer")
     *
     * @param Transaction $transactionInfo
     * @param SerializerHelper $serializerHelper
     * @return JsonResponse
     */
    public function detail(Transaction $transaction, SerializerHelper $serializerHelper): ?JsonResponse
    {
        $transactionInfo = [
            'id' => $transaction->getId(),
            'amount' => $transaction->getAmount(),
            'operation_type' => $transaction->getOperationType()->getName(),
            'transmitter' => [
                'account_number' => $transaction->getTransmitter()->getAccountNumber(),
                'account_type' => $transaction->getTransmitter()->getAccountType()->getName(),
                'user_id' => $transaction->getTransmitter()->getId(),

            ],
            'receiver' => [
                'account_number' => $transaction->getReceiver()->getAccountNumber(),
                'account_type' => $transaction->getReceiver()->getAccountType()->getName(),
                'user_id' => $transaction->getReceiver()->getId(),
            ]
        ];

        return new JsonResponse($serializerHelper->serialize($transactionInfo), 200);
    }
}
