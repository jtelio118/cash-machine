<?php

namespace App\Controller\Api\Transaction;

use App\Entity\BankAccount;
use App\Entity\Transaction;
use App\Entity\TransactionType;
use App\Service\CashMachine;
use App\Service\SerializerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * @Route("/transaction")
 */
class CreateController extends AbstractController
{
    /**
     * Create Transaction
     *
     * @Route("", name="api_transaction_operation", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="transaction",
     *     in="body",
     *     @SWG\Property(property="transaction", type="object",
     *         @SWG\Property(
     *             property="type",
     *             type="string",
     *             description="Type of transactions (1 = Charge, 2 = Pay, 3 = Deposit, 4 = Withdrawals)",
     *             enum={1,2,3,4}
     *         ),
     *         @SWG\Property(
     *             property="amount",
     *             description="Quantity",
     *             type="integer",
     *         ),
     *         @SWG\Property(
     *             property="transmitter",
     *             description="Issuer account ID",
     *             type="integer"
     *         ),
     *         @SWG\Property(
     *             property="receiver",
     *             description="Recipient account Id",
     *             type="integer",
     *         )
     *     )
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Create transaction",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *     )
     * )
     * @SWG\Tag(name="Transactions")
     * @Security(name="Bearer")
     *
     * @param Request $request
     * @param CashMachine $cm
     * @param EntityManagerInterface $em
     * @param SerializerHelper $sh
     * @return JsonResponse
     */
    public function create(
        Request $request,
        CashMachine $cm,
        SerializerHelper $sh,
        EntityManagerInterface $em): ?JsonResponse
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();

        $rules = [
            'type' => new Assert\Choice([1, 2, 3, 4]),
            'amount' => new Assert\Positive(),
        ];

        if (isset($data['transmitter']) && !empty($data['transmitter'])) {
            $rules['transmitter'] = new Assert\NotBlank();
        }

        if (isset($data['receiver']) && !empty($data['receiver'])) {
            $rules['receiver'] = new Assert\NotBlank();
        }

        $constraint = new Assert\Collection($rules);
        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse($sh->serialize([
                'error' => (string) $violations,
            ]), 422);
        }

        try {

            $transmitter = isset($data['transmitter']) ? $this->_getBankAccount($data['transmitter']) : null;
            $receiver = isset($data['receiver']) ? $this->_getBankAccount($data['receiver']) : null;

            $cm->doOperation(
                $this->_getOperationType($data['type']),
                $data['amount'],
                $transmitter,
                $receiver
            );

        } catch (\Exception $e) {
            return new JsonResponse($sh->serialize([
                'error' => $e->getMessage(),
            ]), 500);
        }

        return new JsonResponse($sh->serialize([
            'error' => 'The transaction was successful.',
        ]), 201);
    }

    /**
     * Get Transaction Type
     * @param  int $typeId
     * @return ManagerRegistry
     */
    protected function _getOperationType($typeId)
    {
        $repository = $this->getDoctrine()->getRepository(TransactionType::class);
        return $repository->find($typeId);
    }

    /**
     * Get BankAccount
     * @param  int $accountId
     * @return ManagerRegistry
     */
    protected function _getBankAccount($accountId)
    {
        $repository = $this->getDoctrine()->getRepository(BankAccount::class);
        return $repository->find($accountId);
    }
}
