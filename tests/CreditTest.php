<?php

namespace App\Tests\Util;

use App\Entity\AccountType;
use App\Entity\BankAccount;
use App\Entity\TransactionType;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreditTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Psr\Log\LoggerInterface;
     */
    private $logger;

    /**
     * @var \App\Service\CashMachine
     */
    private $cm;

    /**
     * @var \App\Entity\User
     */
    private $user;

    /**
     * @var \App\Entity\TransactionType
     */
    private $transactionType;

    /**
     * @var \App\Entity\AccountType
     */
    private $accountType;

    /**
     * @var \App\Entity\BankAccount
     */
    private $transmitter;

    protected function setUp()
    {
        self::bootKernel();

        $this->cm = self::$container->get('App\Service\CashMachine');
        $this->entityManager = self::$container->get('doctrine')->getManager();

        $this->logger = self::$container->get('logger');

        $this->_init();
    }

    protected function _init()
    {
        $this->user = new User();
        $this->user->setName('Foddie Inc')
            ->setUsername('fuddie')
            ->setPlainPassword(123456)
            ->setEmail('fuddie@fuddie.com')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setSuperAdmin(false);

        $this->accountType = new AccountType();
        $this->accountType->setName('Credit');

        $this->transactionType = new TransactionType();
        $this->transactionType->setName('Pay');

        $this->transmitter = new BankAccount();
        $this->transmitter->setAccountNumber(10000002)
            ->setBalance(0.00)
            ->setStatus(true)
            ->setUser($this->user)
            ->setAccountType($this->accountType);

        $transactionTypeRepository = $this->createMock(ObjectRepository::class);
        $transactionTypeRepository->expects($this->any())
            ->method('find')
            ->willReturn($this->transactionType);

        $transactionTypeObjectManager = $this->createMock(ObjectManager::class);
        $transactionTypeObjectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($transactionTypeRepository);

        $bankAccountRepository = $this->createMock(ObjectRepository::class);
        $bankAccountRepository->expects($this->any())
            ->method('find')
            ->willReturn($this->transmitter);

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($bankAccountRepository);

    }

    /**
     * Successful Withdrawal
     */
    public function testSuccessfulWithdrawal()
    {
        $this->transactionType->setName('Withdraw');
        $this->transmitter->setBalance(1100.00);

        $this->cm->doOperation($this->transactionType, 1000.00, $this->transmitter);

        // $this->logger->info('' . $this->transmitter->getBalance());
        $this->assertEquals(0, $this->transmitter->getBalance());
    }

    /**
     * Withdraw Insufficient balance.
     *
     * @expectedException \Exception
     */
    public function testInsufficientBalanceWithdrawal()
    {
        $this->transactionType->setName('Withdraw');
        $this->transmitter->setBalance(0.00);

        $this->cm->doOperation($this->transactionType, 1000.00, $this->transmitter);
        $this->expectException(\Exception::class);
        $this->expectOutputString('Insufficient balance.');

    }

    /**
     * Withdraw Quantity can not be less than 0.
     *
     * @outputBuffering disabled
     * @expectedException Exception
     */
    public function testQuantityNotBeLessThanZeroWithdrawal()
    {
        $this->transactionType->setName('Withdraw');
        $this->transmitter->setBalance(1000.00);

        $this->cm->doOperation($this->transactionType, -10.00, $this->transmitter);

        $this->expectException(\Exception::class);
        $this->expectOutputString('The quantity can not be less than 0.');

    }

    /**
     * Withdraw You can only pay with a credit account.
     *
     * @outputBuffering disabled
     * @expectedException Exception
     */
    public function testYouCanOonlyPayWithACreditAccountWithdrawal()
    {

        $this->accountType->setName('Debit');
        $this->transactionType->setName('Pay');

        $this->transmitter->setAccountType($this->accountType);
        $this->transmitter->getAccountType();
        $this->transmitter->setBalance(1000.00);

        $this->cm->doOperation($this->transactionType, -10.00, $this->transmitter);

        $this->expectException(\Exception::class);
        $this->expectOutputString('The quantity can not be less than 0.');

    }

    /**
     * Successful Withdraw
     */
    public function testSuccessfulWithdraw()
    {
        $this->transactionType->setName('Withdraw');
        $this->transmitter->setBalance(1150.00);

        $this->cm->doOperation($this->transactionType, 1000, $this->transmitter);

        $this->assertEquals(50, $this->transmitter->getBalance());
    }

    /**
     * Withdraw Quantity can not be less than 0.
     *
     * @expectedException \Exception
     */
    public function testQuantityNotBeLessThanZeroWithdraw()
    {
        $this->transactionType->setName('Withdraw');
        $this->transmitter->setBalance(0.00);

        $this->cm->doOperation($this->transactionType, 0.00, $this->transmitter);
        $this->expectException(\Exception::class);

    }

    /**
     * Charge Transaction not allowed for account type.
     *
     * @expectedException \Exception
     */
    public function testPayTransactionNotAllowedForAccountTypeDeposit()
    {
        $this->transactionType->setName('Charge');

        $this->cm->doOperation($this->transactionType, 100.00, $this->transmitter);
        $this->expectException(\Exception::class);

    }

    /**
     * Deposit Transaction not allowed for account type.
     *
     * @expectedException \Exception
     */
    public function testChargeTransactionNotAllowedForAccountTypeDeposit()
    {
        $this->transactionType->setName('Deposit');

        $this->cm->doOperation($this->transactionType, 100.00, $this->transmitter);
        $this->expectException(\Exception::class);

    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
