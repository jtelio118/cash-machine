#!/bin/bash

set -e
cd /application

echo "==> Install Fixtures"

echo "==> Executing: Drop && Create Database"
php bin/console doctrine:database:drop --if-exists --force
php bin/console doctrine:database:create
echo "==> Executing: Create Schema"
php bin/console doctrine:schema:create
echo "==> Executing: Run Fixtures"
php bin/console doctrine:fixtures:load --env=dev --no-interaction

echo "==> Finish ..."
