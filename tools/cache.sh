#!/bin/bash

set -e
cd /application

echo "==> Remove Cache"

if [ "$1" == "prod" ]; then
    echo "==> Environment: Production"
    rm -R -f var/cache/* var/log/*

    php bin/console cache:clear --env=prod --no-debug
    php bin/console assets:install
    chmod -R 777 var/cache var/log

elif [ "$1" == "dev" ]; then

    echo "==> Environment: Development"
    php bin/console cache:clear
    php bin/console assets:install --symlink
    chmod -R 777 var/cache var/log

else
  echo "==> Parameter Expected:   "
  echo "                          "
  echo "    dev       Development "
  echo "    prod      Production  "
  echo "                          "
fi

echo "==> Finish ..."
