#!/bin/bash

set -e
cd /application

echo "==> Reset App..."
echo "==> Executing: Drop && Create Database"
php bin/console doctrine:database:drop --if-exists --force
php bin/console doctrine:database:create
echo "==> Executing: Create Schema"
php bin/console doctrine:schema:create
echo "==> Finish ..."
