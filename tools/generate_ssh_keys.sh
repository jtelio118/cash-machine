#!/bin/bash

set -e
cd /application

echo "==> Create folder in: config/jwt"
mkdir -p config/jwt

echo "==> Generate SSH keys:"
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -pass pass:"$SSH_KEYS"
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout -passin pass:"$SSH_KEYS"

echo "==> Executing: Clear Cache"
php bin/console cache:clear

echo "==> Finish ..."
