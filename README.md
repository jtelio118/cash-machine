symfony4-fuddie
========

A Symfony 4 project, with an API skeleton using JWT for user authentication.

## Installation

Run the containers

`docker-compose up -d`

Run container shell for php-fpm

`docker-compose exec php-fpm bash`

And install all the dependencies

`composer install`

Open your browser in `http://localhost:8000/`

#### Create ssh keys

You can do this by running the next command, which will create ssh keys.

`bash tools/generate_ssh_keys.sh`

**Note: Set environment var in app.env file.

#### Creating the database schema

You can do this by running the next command, which will create a clean database and schema.

`bash tools/clean-install.sh`

**Note: This script will actually delete any database that's already created, so be careful when using this.

#### Install Database and Fixtures

If you want to create the database with some fixtures or random data, you can run the next command

`bash tools/install-fixtures.sh`

**Note: This script will actually delete any database that's already created, so be careful when using this.


## Clear Cache

Run the next command with the environment parameter (dev or prod), to clean cache.

`bash tools/cache.sh`

## Tests
Run Shell PHP container

`docker-compose exec php-fpm bash`
 
 Run all Test

`./bin/phpunit`

## Postman

Import Fuddie_postman_collection.json to Postman for execute all request.

**Note: For more info visit next link [Install Postman and Import Request Collection](https://developer.ft.com/portal/docs-start-install-postman-and-import-request-collection)
